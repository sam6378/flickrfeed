import { browser, element, by } from 'protractor';

export class HolidayExtrasFrontendPage {
  navigateTo() {
    return browser.get('/');
  }

  getNoResultsText() {
    return element(by.css('app-root .photo-list__no-results')).getText();
  }

  inputSearch(searchTerm: string) {
    return element(by.css('app-root .photo-list__search')).sendKeys(searchTerm);
  }

  getResults() {
    return element(by.css('app-root .photo-list li:first-child'));
  }

  waitForResults() {
    return browser.wait(() => {
      return browser.isElementPresent(by.css('.photo-list'));
    }, 50000);
  }
}
