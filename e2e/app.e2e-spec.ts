import { HolidayExtrasFrontendPage } from './app.po';

describe('holiday-extras-frontend App', () => {
  let page: HolidayExtrasFrontendPage;

  beforeEach(() => {
    page = new HolidayExtrasFrontendPage();
  });

  it('should display message saying "Please enter a tag name"', () => {
    page.navigateTo();
    expect(page.getNoResultsText()).toEqual('Please enter a tag name');
  });

  it('should display results', () => {
    page.navigateTo();
    page.inputSearch('Holiday');
    page.waitForResults().then(result => {
      expect(page.getResults()).toBeDefined();
    });
  });
});
