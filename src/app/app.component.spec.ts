import { TestBed, async } from '@angular/core/testing';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';

import { PhotoCardComponent } from './photo-card/photo-card.component';
import { PhotoListComponent } from './photo-list/photo-list.component';

import { FlickrService } from './flickr.service';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        PhotoCardComponent,
        PhotoListComponent
      ],
      providers: [
        FlickrService
      ],
      imports: [
        HttpModule
      ]
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
