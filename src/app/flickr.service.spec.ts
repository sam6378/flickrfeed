import { HttpModule, Http, XHRBackend, Response, ResponseOptions } from '@angular/http';
import { TestBed, inject, async } from '@angular/core/testing';
import { MockBackend, MockConnection } from '@angular/http/testing';

import { FlickrService } from './flickr.service';

import { Photo } from './photo';

const makePhotoData = () => [
  {"description":{"_content":"Viernes 14.07.2017"},"farm":5,"id":"35643183860","isfamily":0,"isfriend":0,"ispublic":1,"owner":"141405882@N05","ownername":"imlfernandez","secret":"15849ddfb4","server":"4303","tags":"festival losalamos beach party edm music martingarrix fireworks malaga techno fire fisheye tilllate edmphotography losalamosbeachfestival sander van doorn chuckie brian cross sunnery james ryan marciano","title":"Los Alamos Beach Festival"},
  {"description":{"_content":"Viernes 14.07.2017"},"farm":5,"id":"36032750545","isfamily":0,"isfriend":0,"ispublic":1,"owner":"141405882@N05","ownername":"imlfernandez","secret":"28e84e8e35","server":"4321","tags":"festival losalamos beach party edm music martingarrix fireworks malaga techno fire fisheye tilllate edmphotography losalamosbeachfestival sander van doorn chuckie brian cross sunnery james ryan marciano","title":"Los Alamos Beach Festival"},
  {"description":{"_content":"Viernes 14.07.2017"},"farm":5,"id":"35222624673","isfamily":0,"isfriend":0,"ispublic":1,"owner":"141405882@N05","ownername":"imlfernandez","secret":"75179f9f21","server":"4302","tags":"festival losalamos beach party edm music martingarrix fireworks malaga techno fire fisheye tilllate edmphotography losalamosbeachfestival sander van doorn chuckie brian cross sunnery james ryan marciano","title":"Los Alamos Beach Festival"}
] as Photo[];

describe('FlickrService', () => {

  beforeEach( async(() => {
    TestBed.configureTestingModule({
      imports: [ HttpModule ],
      providers: [
        FlickrService,
        { provide: XHRBackend, useClass: MockBackend }
      ]
    })
    .compileComponents();
  }));

  it('can provide the mockBackend as XHRBackend',
    inject([XHRBackend], (backend: MockBackend) => {
      expect(backend).not.toBeNull('backend should be provided');
  }));

  it('should create', inject([FlickrService], (flickrService: FlickrService) => {
    expect(flickrService).toBeTruthy();
  }));

  describe('when getParams', () => {
    it('return query params from a set of defaults and overrides', inject([FlickrService], (flickrService: FlickrService) => {
      let defaults = {one: '1',two: '2',three: '3'};
      let overrides = {one: 'one',four: 'four'};
      let result = flickrService.getParams(defaults, overrides);
      expect(result).toEqual('one=one&two=2&three=3&four=four');
    }));
  });

  describe('when get', () => {

  let backend: MockBackend;
  let flickrService: FlickrService;
  let fakePhotos: Photo[];
  let response: Response;

  beforeEach(inject([Http, XHRBackend], (http: Http, be: MockBackend) => {
    backend = be;
    flickrService = new FlickrService(http);
    fakePhotos = makePhotoData();
    let options = new ResponseOptions({status: 200, body: fakePhotos});
    response = new Response(options);
  }));

  it('should have expected fake photos', async(inject([], () => {
    backend.connections.subscribe((mockConnection: MockConnection) => mockConnection.mockRespond(response));

    flickrService.get({tags: 'Holiday', per_page: 3})
      .subscribe((result: any) => {
        expect(result.results.length).toBe(3, 'should have expected no. of tracks');
      });
  })));
});
});
