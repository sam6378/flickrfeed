import { Component, OnInit, Input } from '@angular/core';

import { Photo } from '../photo';

@Component({
  selector: 'app-photo-card',
  templateUrl: './photo-card.component.html',
  styleUrls: ['./photo-card.component.scss']
})
export class PhotoCardComponent implements OnInit {

  @Input("photo") photo: Photo = new Photo();

  constructor() { }

  ngOnInit() { }
}
