import { Component, OnInit } from '@angular/core';
import * as Rx from 'rxjs';

import { FlickrService } from '../flickr.service';

import { Photo } from '../photo';

@Component({
  selector: 'app-photo-list',
  templateUrl: './photo-list.component.html',
  styleUrls: ['./photo-list.component.scss']
})
export class PhotoListComponent implements OnInit {

  searchTermSubject: Rx.Subject<string> = new Rx.Subject();
  photos: Photo[] = [];

  constructor(
    private flickrService: FlickrService
  ) { }

  ngOnInit() {
    this.searchTermSubject.debounceTime(500).subscribe(searchTextValue => {
      this.handleSearch(searchTextValue);
    });
  }

  onKeyUp(event: any){
    this.searchTermSubject.next(event.target.value);
  }

  handleSearch(tag) {
    if(tag){
      this.flickrService.get({tags: tag})
        .subscribe((result: Photo[]) => this.photos = result, console.error);
    } else {
      this.photos = [];
    }
  }
}
