import { Http, URLSearchParams } from '@angular/http';
import { Injectable } from '@angular/core';

import 'rxjs/Rx';
import * as Rx from 'rxjs/Rx';

import { GLOBALS } from './globals';

import { Photo } from './photo';

const defaultGetParams = {
  api_key: GLOBALS.FLICKR_API_KEY,
  format: 'json',
  method: 'flickr.photos.search',
  extras: 'description,owner_name,tags',
  per_page: 10,
  nojsoncallback: '?',
  safe_search: 1,
  content_type: 1,
  tags: 'holiday'
};

@Injectable()
export class FlickrService {

  constructor(public http: Http) { }

  get(customParams: any = {}): Rx.Observable<any> {
    return this.http.get(GLOBALS.FLICKR_URL + '?' + this.getParams(defaultGetParams, customParams))
      .map((res: any) => {
        let photos = JSON.parse(res._body).photos.photo;
        return photos.map(photo => new Photo(photo.description, photo.farm, photo.id,
          photo.isfamily, photo.isfriend, photo.ispublic, photo.owner, photo.ownername,
          photo.secret, photo.server, photo.tags, photo.title));
      })
      .catch(this.handleError);
  }

  getParams(defaultParams: any, customParams: any = {}) {
    let options = Object.assign(defaultParams, customParams);

    let params = new URLSearchParams();
    for(let key in options){
      params.set(key, options[key])
    }
    return params.toString();
  }

  private handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body: any = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    return Promise.reject(errMsg);
  }
}
