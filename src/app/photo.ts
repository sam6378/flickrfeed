export class Photo {
  constructor(
    public description: any = {},
    public farm: number = 0,
    public id: string = '',
    public isfamily: number = 0,
    public isfriend: number = 0,
    public ispublic: number = 0,
    public owner: string = '',
    public ownername: string = '',
    public secret = '',
    public server: string = '',
    public tags: string = '',
    public title = '',
  ) { }
}
